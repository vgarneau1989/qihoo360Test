package main

import (
	"flag"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/masato25/qihoo360Test/app/controller"
	clib "github.com/masato25/qihoo360Test/consul/lib"
	log "github.com/sirupsen/logrus"
)

var consulServer = flag.String("consul_host", "0.0.0.0:8000", "consul api setup")
var host = flag.String("host", "0.0.0.0", "public ip of this web server")
var port = flag.Int("port", 8088, "web service open port")
var registerConsul = flag.Bool("consul_register", false, "register serives with consul server")
var debugMode = flag.Bool("debug", false, "open debug mode")

func main() {
	flag.Parse()
	if *debugMode {
		log.SetLevel(log.DebugLevel)
		log.Info("debug mode: open")
	} else {
		// release mode
		gin.SetMode("release")
	}
	r := gin.Default()
	controller.Routes(r)
	if *registerConsul {
		log.Infof("will regist service with consul -> %s", *consulServer)
		clib.ServiceRegister(*consulServer, fmt.Sprintf("%s:%d", *host, *port))
	}
	r.Run(fmt.Sprintf(":%d", *port))
}

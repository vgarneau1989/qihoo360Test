package computing_servies

import (
	"github.com/gin-gonic/gin"
)

func Route(r *gin.Engine) {
	r.GET("/", Index)
	r.POST("/upper", UpperHandler)
	r.POST("/select", SelectWordsHandler)
}

package computing_servies

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestHelpFunctions(t *testing.T) {
	Convey("test upper map values", t, func() {
		testdata := map[string]string{"bar": "a@Fb", "foo": "ab!Cd"}
		result := UpperMapValues(testdata)
		So(result["bar"], ShouldEqual, "A@FB")
		So(result["foo"], ShouldEqual, "AB!CD")
	})
	Convey("test select values", t, func() {
		testdata := map[string]int{"Apple": 3, "Foo": 1, "Bar": 5}
		result := SelectWeight(testdata, 0)
		So(result, ShouldEqual, "Bar")
		result = SelectWeight(testdata, 1)
		So(result, ShouldEqual, "Foo")
	})
}

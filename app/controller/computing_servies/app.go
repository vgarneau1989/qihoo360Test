package computing_servies

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func Index(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "ok",
	})
	return
}

func UpperHandler(c *gin.Context) {
	var parseInputBody map[string]string
	data, _ := ioutil.ReadAll(c.Request.Body)
	log.Debugf("[UpperHandler] received body: %v", data)
	err := json.Unmarshal(data, &parseInputBody)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, UpperMapValues(parseInputBody))
}

func SelectWordsHandler(c *gin.Context) {
	var WeightSelectType int
	sortBy := c.GetHeader("sort_by")
	if selectType, err := strconv.Atoi(sortBy); err == nil {
		WeightSelectType = selectType
	}
	log.Debugf("[SelectWordsHandler] WeightSelectType: %d", WeightSelectType)
	var parseInputBody map[string]int
	data, _ := ioutil.ReadAll(c.Request.Body)
	log.Debugf("[SelectWordsHandler] received body: %v", data)
	err := json.Unmarshal(data, &parseInputBody)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, SelectWeight(parseInputBody, WeightSelectType))
}

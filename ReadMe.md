# Qihoo360Test

Run with docker
```
cd docker
docker-compose build goweb
docker-compose build consul
docker-compose up -d goweb
```

Build from source
```
# install dep https://github.com/golang/dep
go get -u github.com/golang/dep/cmd/dep
mkdir -p $GOPATH/src/github.com/masato25
cd $GOPATH/src/github.com/masato25
git clone https://gitlab.com/masato25/qihoo360Test.git
cd qihoo360Test
# install dependences
$GOPATH/bin/dep ensure
go build
./qihoo360Test
```

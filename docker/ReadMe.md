# Experiment docker network

* docker-compose building
```
# build all
docker-compose build
# build one container
docker-compose build goweb
```

* docker-compose start
```
docker-compose up -d goweb
docker-compose up -d nginx0
```

![](./qiho360T.png)


nginx會透過 [nginx-upsync-module](https://github.com/weibocom/nginx-upsync-module) 和 consul的 key value store service去做服務發現.
* 這邊每一個web service啟動時可以透過參數去和consul server去宣告自己所在的位置.
  * ex ./qihoo360Test -host goweb -port 8088 -consul_host dev-consul:8500 -consul_register=true
  * 上述例子設定host和port組成value然後向dev-consul:8500的consul api server去註冊服務
* nginx可以透過這個key value list將請求去轉送給註冊在consul服務下的服務器

package main

import (
	"flag"

	"github.com/masato25/qihoo360Test/consul/consul_app/g"
	"github.com/masato25/qihoo360Test/consul/consul_app/utility"
)

var consulhost = flag.String("consulhost", "0.0.0.0:8500", "consul server ip + port")
var serviceHost = flag.String("shost", "mygo", "service ip or domain")
var servicePort = flag.Int("sport", 8088, "service port")
var serviceName = flag.String("sname", "mygo2", "service register name")

func main() {
	flag.Parse()
	g.Set("ashi", ".")
	utility.RegisterService(consulhost, *serviceHost, *serviceName, *servicePort)
}

package lib

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

func ServiceRegister(consulApi string, serviceDomain string) {
	client := &http.Client{}
	urI := fmt.Sprintf("http://%s/v1/kv/upstreams/web/%s", consulApi, serviceDomain)
	log.Infof("[PUT] %s", urI)
	req, err := http.NewRequest("PUT", urI, strings.NewReader(""))
	if err != nil {
		log.Error(err.Error())
		return
	}
	resp, err := client.Do(req)
	defer resp.Body.Close()
	if err != nil {
		log.Error(err.Error())
		return
	}
	body, _ := ioutil.ReadAll(resp.Body)
	log.Infof("register successed with body: %v", string(body))
}

package main

import (
	"flag"
	"fmt"

	"github.com/masato25/qihoo360Test/benchmark_tool/utility"
	log "github.com/sirupsen/logrus"
)

var domain = flag.String("domain", "localhost:8088", "test request domain(including port)")
var urlpath = flag.String("urlpath", "/", "urlpath of request api")
var requestMethod = flag.String("methods", "GET", "request method ex. GET POST PUT DELETE")
var reqBody = flag.String("body", "", "request body of request")

func main() {
	flag.Parse()
	log.Infof("request url: %s%s , request method: %s, request body: %s", *domain, *urlpath, *requestMethod, *reqBody)
	utility.CallHttpRequest(*domain, fmt.Sprintf("http://%s%s", *domain, *urlpath), *requestMethod, *reqBody)
}

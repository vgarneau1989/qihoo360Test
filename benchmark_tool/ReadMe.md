# Qihoo360Test Tool

Build command from source:
```
cd ..
dep ensure
cd ./benchmark_tool
go build
```

Use Docker container:
```
cd ../docker
docker-compose build benchmark_tool
docker-compose run benchmark_tool
```

Support options:
```
╰─$ ./benchmark_tool --help                                                     
Usage of ./benchmark_tool:
  -body string
        request body of request
  -domain string
        test request domain(including port) (default "localhost:8088")
  -methods string
        request method ex. GET POST PUT DELETE (default "GET")
  -urlpath string
        urlpath of request api (default "/")

```

Test case example:
```
benchmark_tool -domain goweb:8088 --urlpath "/upper" --methods "POST" --body "{\"a\":\"bb\"}"
benchmark_tool -domain goweb:8088 --urlpath "/select" --methods "POST" --body "{\"Apple\": 2,\"Foo\": 3}"
```
* test with docker network, please run `docer-compose up -d goweb` before you run `docker-compose run benchmark_tool`
* if you want test benchmark with public online web server, please replace domain flag into online domain. 

output example:
```
INFO[0000] request url: localhost:8088/upper , request method: POST, request body: {"a":"bb"}
Requests/sec:           4602.32
Transfer/sec:           521.36KB
Avg Req Time:           434.563µs
Fastest Request:        216.733µs
Slowest Request:        6.170787ms
Number of Errors:       0
44177 requests in 9.598861986s, 4.89MB read
```
